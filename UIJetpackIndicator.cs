﻿using UnityEngine;
using UnityEngine.UI;

public class UIJetpackIndicator : MonoBehaviour
{
    public Image jetpackBar;

    private PlayerMovement player;

    public static UIJetpackIndicator instance;

    private void Start()
    {
        instance = this;
    }

    private void Update()
    {
        if (player == null) return;

        float jetpackFactor = player.GetCurrentJetpackDuration() / player.jetpackDuration;

        jetpackBar.fillAmount = jetpackFactor;
    }

    public void SetPlayer(PlayerMovement player)
    {
        this.player = player;
    }
}
