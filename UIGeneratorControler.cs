﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGeneratorControler : MonoBehaviour
{
    public GameObject panel;
    public InventorySlotUI slot;
    public Transform resultSlotParent;
    [SerializeField] private InventorySlotUI[] resultSlots;
    public Button onButton;
    public Button offButton;
    public Text activeText;

    public Image fuelBar;
    public Image fuelImage;

    public Image resBar;
    public Image resImage;


    public FuelUsage usage;
    public Generator generator;

    public static UIGeneratorControler instance;

    private void Start()
    {
        instance = this;

        resultSlots = resultSlotParent.GetComponentsInChildren<InventorySlotUI>();
    }

    private void Update()
    {
        if (usage != null && panel.activeSelf && usage.active) UpdateProcess();
    }

    private void UpdateProcess()
    {
        fuelImage.sprite = usage.fuelType.image;
        fuelBar.fillAmount = usage.FilledFactor();

        resImage.sprite = generator.result.item.image;
        resBar.fillAmount = generator.FilledFactor();
    }

    public void Open(FuelUsage usage)
    {
        this.usage = usage;
        this.generator = usage.GetComponent<Generator>();

        UIInventoryControler.instance.panel.SetActive(true);
        panel.SetActive(true);
        UpdateUI();
    }

    public void Close()
    {
        usage = null;
        panel.SetActive(false);
    }

    public void TurnOn()
    {
        usage.GetComponent<Generator>().TurnOn();
        Close();
    }

    public void TurnOff()
    {
        usage.GetComponent<Generator>().TurnOff();
        Close();
    }

    public void UpdateUI()
    {
        if (usage == null) return;
        InventoryItem invItem = null;
        if (!usage.HasFuel()) invItem = new InventoryItem(null, 0);
        else invItem = new InventoryItem(usage.currentFuel.item, usage.currentFuel.amount);
        Debug.Log(invItem);
        Debug.Log(usage);
        slot.SetItem(invItem);
        onButton.interactable = !usage.IsActive();
        offButton.interactable = usage.IsActive();
        activeText.text = usage.IsActive() ? "Active" : "Inactive";
        activeText.color = usage.IsActive() ? Color.green : Color.red;

        for (int i = 0; i < resultSlots.Length; i ++)
        {
            if (i < generator.inventory.Count) resultSlots[i].SetItem(new InventoryItem(generator.inventory[i].item, generator.inventory[i].amount));
            else resultSlots[i].Disable();
        }
    }
}
