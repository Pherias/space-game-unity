﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIGeneratorAction : Action
{
    private ItemCardinality item;
    public override void Initialize(ItemCardinality item)
    {
        this.item = item;
    }

    public void Use()
    {
        if (UIGeneratorControler.instance.panel.activeSelf) UIGeneratorControler.instance.usage.ReduceFuel();
        else if (UIDrillerControler.instance.panel.activeSelf) UIDrillerControler.instance.usage.ReduceFuel();

        Inventory.local.Add(item.item, item.amount);

        UIInventoryControler.instance.UpdateUI();
        UIGeneratorControler.instance.UpdateUI();

        Cancel();
    }

    public void Cancel()
    {
        Destroy(gameObject);
    }
}
