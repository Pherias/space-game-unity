﻿using UnityEngine;
using UnityEngine.UI;

public class Notification : MonoBehaviour
{
    public Text text;

    private void Start()
    {
        text = GetComponent<Text>();
    }

    private void Update()
    {
        transform.position += new Vector3(0, 50 * Time.deltaTime, 0);
    }
}
