﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinePiece : MonoBehaviour
{
    private void Start()
    {
        Planet planet = Planet.GetClosest(gameObject.transform.position);

        GetComponent<SpriteRenderer>().color = planet.horizonColor;
    }
}
