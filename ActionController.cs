﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionController : MonoBehaviour
{
    private PhotonView view;
    private Camera main;
    public static ActionController local;

    private void Start()
    {
        view = GetComponent<PhotonView>();
        main = MainCamera.instance.GetComponent<Camera>();

        if (view.isMine) local = this;
    }

    private void Update()
    {
        if (!view.isMine) return;

        HandleMouse();
    }

    private void HandleMouse()
    {

        Vector3 mousePos = main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

        Collider2D[] hits = Physics2D.OverlapPointAll(mousePos2D);
            
        foreach (Collider2D hit in hits)
        {
            Hover(hit.transform.gameObject, mousePos2D);
        }

        if (hits.Length == 0) SetCursor(null);

    }

    private void Hover(GameObject obj, Vector2 position)
    {
        Interactable interaction = obj.GetComponent<Interactable>();

        if (interaction == null) return;

        SetCursor(interaction.GetTexture());

        if (Input.GetMouseButton(0)) Hold(interaction);
        if (Input.GetMouseButtonDown(0)) Click(interaction);
    }

    public void Click(Interactable interaction)
    {
        if (CanInteractWith(interaction)) interaction.Interact();
    }

    private bool CanInteractWith(Interactable interaction)
    {
        PhotonView interactionView = interaction.GetComponent<PhotonView>();

        if (interactionView == null) return true;
        else return interactionView.isMine;
    }

    public void Hold(Interactable interaction)
    {
        if (interaction is Minable)
        {
            Minable m = (Minable)interaction;

            if (m.Mine(1f)) {
                m.AfterMine();
                view.RPC("RPC_Mine", PhotonTargets.AllBuffered, m.transform.position);
            }
        }
    }

    public static void SetCursor(Texture2D cursor)
    {
        Cursor.SetCursor(cursor, Vector2.zero, CursorMode.Auto);
    }


    [PunRPC]
    private void RPC_Mine(Vector3 position)
    {
        Collider2D[] hits = Physics2D.OverlapPointAll(position);

        foreach(Collider2D hit in hits)
        {
            if (hit.GetComponent<Minable>() != null)
            {
                Destroy(hit.gameObject);
            }
        }
    }
}
