﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Assets : MonoBehaviour
{
    public static Assets instance;

    public GameObject minePiece;
    public GameObject planetPiece;

    public AudioClip backgroundMusic;

    private AudioSource player;

    private void Start()
    {
        player = GetComponent<AudioSource>();
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public void PlayBackgroundMusic()
    {
        player.clip = backgroundMusic;
        player.Play();
        player.loop = true;
    }
}
