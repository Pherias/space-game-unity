﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public bool inPlanetRange;
    public float speed;
    public float jetpackForce;
    public float jumpForce;
    public float rotationSpeed;
    public float jetpackEfficiency;
    public MaterialItem jetpackFuel;

    private bool rotateToPlanet = true;

    public float jetpackDuration;
    private float currentJetpackDuration;

    private float coalTimer;

    private bool grounded = false;

    public GameObject smoke;

    private Rigidbody2D rb;
    private Gravity gravity;
    private PhotonView view;

    private float facing;

    public static PlayerMovement local;

    void Start() {
        rb = GetComponent<Rigidbody2D>();
        gravity = GetComponent<Gravity>();
        view = GetComponent<PhotonView>();

        currentJetpackDuration = jetpackDuration;
        coalTimer = 0f;
        facing = -1;

        if (!view.isMine) return;

        UIJetpackIndicator.instance.SetPlayer(this);
        local = this;
    }

    void Update() {

        HandleMovement();
        HandleKeys();
    }

    private void HandleMovement() {

        if (!view.isMine) return;

        Vector3 moveDir = transform.rotation * Vector3.up;

        // Jetpack movement
        if (Input.GetKey("w") && currentJetpackDuration > 0f && Inventory.local.InventoryHasItem(jetpackFuel) && !rotateToPlanet)
        {
            view.RPC("RPC_AddForce", PhotonTargets.All, moveDir * jetpackForce);
            view.RPC("RPC_ActivateJetpack", PhotonTargets.All);

            currentJetpackDuration -= 2 * Time.deltaTime;
            RunCoalTimer();
        }
        else if (currentJetpackDuration < jetpackDuration)
        {
            currentJetpackDuration += Time.deltaTime;
        }

        if (Input.GetKeyDown("w") && !Inventory.local.InventoryHasItem(jetpackFuel) && !rotateToPlanet) UINotificationControler.instance.SetNotification("You have no fuel!", 1f, Color.white);

        float direction = 0;

        // Rotation
        if (Input.GetKey("q")) direction = 1;
        else if (Input.GetKey("e")) direction = -1;

        view.RPC("RPC_Rotate", PhotonTargets.All, direction);

        if (!inPlanetRange) {
            view.RPC("RPC_Move", PhotonTargets.All, Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"), moveDir);
        } else {
            if (Input.GetAxis("Horizontal") != 0) transform.position += transform.rotation * Vector3.right * Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        }

        Planet closest = gravity.GetClosestPlanet();
        
        if (rotateToPlanet && inPlanetRange)
        {
            
            Vector3 rotateDirection = closest.transform.position - transform.position;
            float angle = Mathf.Atan2(rotateDirection.y, rotateDirection.x) * Mathf.Rad2Deg + 90;

            transform.rotation = Quaternion.Euler(0f, 0f, angle);

            if (Input.GetKey("w") && grounded)
            {
                view.RPC("RPC_Jump", PhotonTargets.All, moveDir * jumpForce);
                grounded = false;
            }
        }

        bool inRange = closest.InRange(gameObject);
        if (inRange) MainCamera.instance.SetBackgroundColor(closest.horizonColor);
        else MainCamera.instance.SetBackgroundColor(Color.black);

        facing = Input.GetAxis("Horizontal");
        if (facing != 0) view.RPC("RPC_SynchronizeAnimation", PhotonTargets.All, facing);
    }

    [PunRPC]
    private void RPC_SynchronizeAnimation(float direction)
    {
        if (direction > 0) GetComponentInChildren<SpriteRenderer>().flipX = true;
        else GetComponentInChildren<SpriteRenderer>().flipX = false;
    }

    [PunRPC]
    private void RPC_AddForce(Vector3 vector)
    {
        GetComponent<Rigidbody2D>().AddForce(vector);
    }
    
    [PunRPC]
    private void RPC_Jump(Vector3 vector)
    {
        GetComponent<Rigidbody2D>().velocity = vector;
    }

    [PunRPC]
    private void RPC_Rotate(float direction)
    {
        transform.RotateAround(transform.position, Vector3.forward, direction * rotationSpeed * Time.deltaTime);
    }

    [PunRPC]
    private void RPC_Move(float y, float x, Vector3 dir)
    {
        transform.position += dir * speed * y * Time.deltaTime;
        transform.position += transform.rotation * Vector3.right * speed * x * Time.deltaTime;
    }

    [PunRPC]
    private void RPC_ActivateJetpack()
    {
        GameObject obj = Instantiate(smoke);
        obj.transform.position = transform.position;
        obj.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 100f));
        Destroy(obj, 2f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        grounded = true;
    }

    private void HandleKeys() {

        if (Input.GetKeyDown(KeyCode.LeftShift)) rotateToPlanet = !rotateToPlanet;
   
    }

    public float GetCurrentJetpackDuration()
    {
        return currentJetpackDuration;
    }

    private void RunCoalTimer()
    {
        coalTimer += Time.deltaTime;

        if (coalTimer >= jetpackEfficiency)
        {
            coalTimer = 0f;
            Inventory.local.Remove(jetpackFuel, 1);
        }
    }
}
