﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : ScriptableObject
{
    public new string name;
    public Sprite image;
    bool canUseInInventory;
    public virtual void UseInInventory(int amount)
    {
        Discard(amount);
    }

    public virtual void Discard(int amount)
    {
        Inventory.local.Remove(this, 1);
    }
}
