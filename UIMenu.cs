﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIMenu : MonoBehaviour
{
    [SerializeField] private string playerName;
    public InputField nameField;

    private void Start()
    {
        nameField = GetComponentInChildren<InputField>();

        CheckUser();
    }

    private void CheckUser()
    {
        string name = PlayerPrefs.GetString("name");

        nameField.text = name;
    }

    public void OnStart()
    {
        if (nameField.text == "") return;

        PlayerPrefs.SetString("name", nameField.text);

        NetworkControler.instance.JoinLobby(nameField.text);
        SceneManager.LoadScene("Space");
    }

}
