﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelUsage : MonoBehaviour
{
    public MaterialItem fuelType;
    public ItemCardinality currentFuel;
    private PhotonView view;

    /**
     * boolean decided wether the object is active and consuming resources. Other component checks this active.
     */
    public bool active;

    /**
     * Efficiency is the time it takes to take one material as fuel. Greater efficiency means less fuel used.
     */
    public float efficiency;
    private float efficiencyTimer;

    private void Start()
    {
        active = false;
        efficiencyTimer = 0f;
        currentFuel = new ItemCardinality(null, 0);

        view = GetComponent<PhotonView>();

    }

    private void Update()
    {
        if (!view.isMine) return;

        if (active) Run();
    }

    private void Run()
    {
        efficiencyTimer += Time.deltaTime;

        if (efficiencyTimer >= efficiency)
        {
            ReduceFuel();
            efficiencyTimer = 0f;
        }
    }

    public bool AddFuel(MaterialItem fuel, int amount)
    {
        if (fuelType.GetType() != fuel.GetType()) return false;
      
        if (currentFuel.item != null && currentFuel.item.GetType() == fuel.GetType())
        {
            currentFuel.amount += amount;
        }
        else
        {
            currentFuel = new ItemCardinality(fuel, amount);
        }

        UpdateUI();

        return true;
    }

    private void UpdateUI()
    {
        if (UIGeneratorControler.instance.panel.activeSelf) UIGeneratorControler.instance.UpdateUI();
        if (UIDrillerControler.instance.panel.activeSelf) UIDrillerControler.instance.UpdateUI();
    }

    public void ReduceFuel()
    {
        currentFuel.amount -= 1;

        if (currentFuel.amount <= 0) SetActive(false);

        UpdateUI();
    }

    public void SetActive(bool active)
    {
        this.active = active;

        if (this.active && !HasFuel())
        {
            this.active = false;
            UINotificationControler.instance.SetNotification(name + " does not have any fuel!", 1f, Color.white);
            return;
        }
        
        view.RPC("RPC_SetActive", PhotonTargets.AllBuffered, active);
    }

    public bool HasFuel()
    {
        if (currentFuel == null || currentFuel.item == null || currentFuel.amount <= 0) return false;
        else return true;
    }

    public float FilledFactor()
    {
        return efficiencyTimer / efficiency;
    }

    public bool IsActive()
    {
        return this.active;
    }

    [PunRPC]
    private void RPC_SetActive(bool active)
    {
        this.active = active;

        if (GetComponent<Animator>() != null) GetComponent<Animator>().SetBool("active", active);
    }
}
