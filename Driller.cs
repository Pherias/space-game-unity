﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(FuelUsage))]
public class Driller : Interactable
{
    private FuelUsage usage;
    private PhotonView view;

    public List<ItemCardinality> inventory;
    public int maxResults;
    public int maxStack;

    public float sideTime;
    public float downTime;

    public float moveSpeed;
    public float mineSpeed;

    private bool movingSideWays;
    private float sidewaysTimer;
    private float direction;

    private bool movingDown;
    private float downTimer;

    public DrillPiece left, right, down;

    private void Start()
    {
        usage = GetComponent<FuelUsage>();
        view = GetComponent<PhotonView>();

        StartDown();
    }

    private void Update()
    {
        if (!view.isMine) return;

        if (usage.IsActive()) HandleMovement();
    }

    private void HandleMovement()
    {
        if (movingDown)
        {
            downTimer -= Time.deltaTime;
            if (downTimer <= 0f)
            {
                StartSideways();
            }
        }
        else if (movingSideWays)
        {
            transform.position += transform.rotation * new Vector3(direction * Time.deltaTime * moveSpeed, 0f, 0f);
            sidewaysTimer -= Time.deltaTime;
            if (sidewaysTimer <= 0f)
            {
                StartDown();
            }
        }
    }

    private void StartDown()
    {
        movingDown = true;
        movingSideWays = false;
        downTimer = downTime;
        left.active = false;
        right.active = false;
        down.active = true;
    }

    private void StartSideways()
    {
        movingDown = false;
        movingSideWays = true;
        sidewaysTimer = sideTime;
        direction = UnityEngine.Random.Range(-1f, 1f);

        left.active = direction < 0;
        right.active = direction > 0;
        down.active = false;
    }

    public void Mine(Minable minable)
    {
        AddResource(minable);

        view.RPC("RPC_Mine", PhotonTargets.AllBuffered, minable.transform.position);
    }

    public void AddResource(Minable minable)
    {

        bool inInv = false;

        foreach (ItemCardinality item in inventory)
        {
            if (item.item == minable.material && item.amount + minable.amount <= maxStack)
            {
                inInv = true;
                item.amount += minable.amount;
            }
        }

        if (!inInv) inventory.Add(new ItemCardinality(minable.material, minable.amount));

        if (InventoryIsFull())
        {
            TurnOff();
        }

        UpdateUI();
    }

    public void RemoveInventory(ItemCardinality item)
    {
        ItemCardinality itemToRemove = null;

        foreach (ItemCardinality invItem in inventory)
        {
            if (invItem.item == item.item)
            {
                itemToRemove = invItem;
            }
        }
        inventory.Remove(itemToRemove);
        UpdateUI();
    }

    public bool InventoryIsFull()
    {
        if (inventory.Count >= maxResults)
        {
            UINotificationControler.instance.SetNotification(this.name + " inventory is full!", 1f, Color.black);
            return true;
        }
        else return false;
    }

    public override void Interact()
    {
        UIDrillerControler.instance.Open(usage);
    }

    public void TurnOn()
    {
        usage.SetActive(true);
    }

    public void TurnOff()
    {
        usage.SetActive(false);
    }

    private void UpdateUI()
    {
        if (UIDrillerControler.instance.panel.activeSelf) UIDrillerControler.instance.UpdateUI();
    }

    [PunRPC]
    private void RPC_Mine(Vector3 position)
    {
        Collider2D[] hits = Physics2D.OverlapPointAll(position);

        foreach (Collider2D hit in hits)
        {
            if (hit.GetComponent<Minable>() != null)
            {
                Destroy(hit.gameObject);
            }
        }
    }
}
