﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{

    public List<InventoryItem> items;

    private PhotonView view;
    public static Inventory local;

    public Item[] inventoryOnStart;

    private void Start()
    {
        view = GetComponent<PhotonView>();

        if (!view.isMine) return;

        items = new List<InventoryItem>();
        local = this;

        for (int i = 0; i < inventoryOnStart.Length; i ++)
        {
            Add(inventoryOnStart[i], 1);
        }

        UpdateUI();
    }

    public void Add(Item item, int amount)
    {

        bool itemInInventory = InventoryHasItem(item);

        if (itemInInventory)
        {
            foreach (InventoryItem i in items)
            {
                if (i.GetItem() == item) i.Increase(amount);
            }
        } 
        else
        {
            InventoryItem newItem = new InventoryItem(item, amount);

            items.Add(newItem);
        }

        UpdateUI();
    }

    public void Remove(Item item, int amount)
    {
        bool itemInInventory = InventoryHasItem(item);

        if (!itemInInventory) return;

        InventoryItem itemToRemove = null;

        foreach (InventoryItem i in items)
        {
            if (i.GetItem() == item)
            {
                if (i.GetAmount() > amount) i.Decrease(amount);
                else itemToRemove = i;
            } 
        }

        if (itemToRemove != null) items.Remove(itemToRemove);

        UpdateUI();
    }

    private void UpdateUI()
    {
        UIInventoryControler.instance.UpdateUI();
        UICraftingControler.instance.UpdateUI(this);
    }

    public bool InventoryHasItem(Item item)
    {
        bool hasItem = false;

        foreach (InventoryItem i in items)
        {
            if (i.GetItem() == item) hasItem = true;
        }

        return hasItem;
    }

    public bool InventoryHasItem(Item item, int amount)
    {
        bool hasItems = false;

        foreach (InventoryItem i in items)
        {
            if (i.GetItem() == item && i.GetAmount() >= amount) hasItems = true;
        }

        return hasItems;
    }
}

public class InventoryItem
{
    Item item;
    int amount;

    public InventoryItem(Item item, int amount)
    {
        this.item = item;
        this.amount = amount;
    }

    public void Increase(int amount)
    {
        this.amount += amount;
    }

    public void Decrease(int amount)
    {
        this.amount -= amount;
    }

    public Item GetItem() => item;
    public int GetAmount() => amount;
}
