﻿using UnityEngine;
using UnityEngine.UI;

public class InventorySlotUI : MonoBehaviour
{
    public Image image;
    public Text number;
    public Image numberBack;
    private ItemCardinality item;

    public static InventorySlotUI dragging;
    public GameObject actionPanel;

    public void SetItem(InventoryItem item)
    {
        if (item.GetItem() == null)
        {
            Disable();
            return;
        }
        image.enabled = true;
        this.item = new ItemCardinality(item.GetItem(), item.GetAmount());
        image.sprite = item.GetItem().image != null ? item.GetItem().image : image.sprite;

        if (item.GetAmount() > 1)
        {
            number.text = item.GetAmount().ToString();
            numberBack.enabled = true;
        }
        else
        {
            number.text = "";
            numberBack.enabled = false;
        }
        
    }

    public void Disable()
    {
        image.enabled = false;
        image.sprite = null;
        number.text = "";
        numberBack.enabled = false;
    }

    int clicks = 0;

    public void Click()
    {
        if (actionPanel == null) return;

        GameObject p = Instantiate(actionPanel, FindObjectOfType<UIInventoryControler>().transform);
        p.transform.position = Input.mousePosition + new Vector3(0, -50f, 0);
        p.GetComponent<Action>().Initialize(item);
        
        /*clicks++;
        if (clicks == 2) DragDrop();
        Invoke("ResetDragDrop", 0.5f);*/
    }

    private void ResetDragDrop()
    {
        clicks = 0;
    }

    private void DragDrop()
    {
        ResetDragDrop();
        dragging = this;
    }

    private void Update()
    {
        if (dragging == this)
        {
            image. transform.position = Input.mousePosition;
        }
    }
}
