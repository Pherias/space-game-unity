﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICraftingControler : MonoBehaviour
{
    public Craftable[] craftable;

    public KeyCode key;
    public GameObject panel;
    public Transform craftablesParent;

    public static UICraftingControler instance;


    private void Start()
    {
        instance = this;
    }

    private void Update()
    {
        if (Input.GetKeyDown(key))
        {
            panel.SetActive(!panel.activeSelf);
            UpdateUI(null);
        }
    }

    public void UpdateUI(Inventory player)
    {
        UICraftableRow[] rs = craftablesParent.GetComponentsInChildren<UICraftableRow>();

        for (int i = 0; i < rs.Length; i++)
        {
            if (i < craftable.Length)
            {
                rs[i].Initialize(craftable[i]);
            } else
            {
                rs[i].Disable();
            }
        }
    }

}
