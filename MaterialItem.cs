﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialItem : Item
{
    public override void UseInInventory(int amount)
    {
        if (UIGeneratorControler.instance.panel.activeSelf)
        {
            if (UIGeneratorControler.instance.usage.AddFuel(this, 1)) Inventory.local.Remove(this, 1);
        }
        else if (UIDrillerControler.instance.panel.activeSelf)
        {
            if (UIDrillerControler.instance.usage.AddFuel(this, 1)) Inventory.local.Remove(this, 1);
        } else UINotificationControler.instance.SetNotification("You can't use that here", .5f, Color.white);
    }
}
