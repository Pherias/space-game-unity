﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(FuelUsage))]
public class Generator : Interactable
{
    public bool active;
    public ItemCardinality result;
    public float duration;
    private float currentDuration;
    
    public List<ItemCardinality> inventory;
    public int maxResults;

    private PhotonView view;
    private FuelUsage fuelUsage;

    private void Start()
    {
        view = GetComponent<PhotonView>();
        fuelUsage = GetComponent<FuelUsage>();

        if (!view.isMine) return;

        currentDuration = 0f;
        inventory = new List<ItemCardinality>();
    }

    private void Update()
    {
        if (!view.isMine) return;

        if (fuelUsage.IsActive()) HandleTimer();
    }

    private void HandleTimer()
    {
        currentDuration += Time.deltaTime;

        if (currentDuration >= duration)
        {
            currentDuration = 0f;
            GiveResource();
        }
    }


    private void GiveResource()
    {
        inventory.Add(result);

        if (InventoryIsFull())
        {
            TurnOff();
        }

        UpdateUI();
    }

    public void RemoveInventory(ItemCardinality item)
    {
        ItemCardinality itemToRemove = null;

        foreach (ItemCardinality invItem in inventory)
        {
            if (invItem.item == item.item)
            {
                itemToRemove = invItem;
            }
        }
        inventory.Remove(itemToRemove);
        UpdateUI();
    }
    private void UpdateUI()
    {
        if (UIGeneratorControler.instance.panel.activeSelf) UIGeneratorControler.instance.UpdateUI();
    }

    public override void Interact()
    {
        UIGeneratorControler.instance.Open(fuelUsage);
    }

    private bool InventoryIsFull()
    {
        if (inventory.Count >= maxResults)
        {
            UINotificationControler.instance.SetNotification(this.name + " inventory is full!", 1f, Color.black);
            return true;
        }
        else return false;
    }

    public float FilledFactor()
    {
        return currentDuration / duration;
    }

    public void TurnOn()
    {
        if (InventoryIsFull()) return;

        fuelUsage.SetActive(true);
    }

    public void TurnOff()
    {
        fuelUsage.SetActive(false);
    }
}
