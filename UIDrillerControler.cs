﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDrillerControler : MonoBehaviour
{
    public GameObject panel;
    public InventorySlotUI slot;
    public Transform resultSlotParent;
    [SerializeField] private InventorySlotUI[] resultSlots;
    public Button onButton;
    public Button offButton;
    public Text activeText;

    public Image fuelBar;
    public Image fuelImage;

    public FuelUsage usage;
    public Driller driller;

    public static UIDrillerControler instance;

    private void Start()
    {
        instance = this;

        resultSlots = resultSlotParent.GetComponentsInChildren<InventorySlotUI>();
    }

    private void Update()
    {
        if (usage != null && panel.activeSelf && usage.active) UpdateProcess();
    }

    private void UpdateProcess()
    {
        fuelImage.sprite = usage.fuelType.image;
        fuelBar.fillAmount = usage.FilledFactor();
    }

    public void Open(FuelUsage usage)
    {
        this.usage = usage;
        this.driller = usage.GetComponent<Driller>();

        UIInventoryControler.instance.panel.SetActive(true);
        panel.SetActive(true);
        UpdateUI();
    }

    public void Close()
    {
        usage = null;
        panel.SetActive(false);
    }

    public void TurnOn()
    {
        usage.GetComponent<Driller>().TurnOn();
        Close();
    }

    public void TurnOff()
    {
        usage.GetComponent<Driller>().TurnOff();
        Close();
    }

    public void UpdateUI()
    {
        if (usage == null) return;
        InventoryItem invItem;
        if (!usage.HasFuel()) invItem = new InventoryItem(null, 0);
        else invItem = new InventoryItem(usage.currentFuel.item, usage.currentFuel.amount);

        slot.SetItem(invItem);
        onButton.interactable = !usage.IsActive();
        offButton.interactable = usage.IsActive();
        activeText.text = usage.IsActive() ? "Active" : "Inactive";
        activeText.color = usage.IsActive() ? Color.green : Color.red;

        for (int i = 0; i < resultSlots.Length; i++)
        {
            if (i < driller.inventory.Count) resultSlots[i].SetItem(new InventoryItem(driller.inventory[i].item, driller.inventory[i].amount));
            else resultSlots[i].Disable();
        }
    }
}
