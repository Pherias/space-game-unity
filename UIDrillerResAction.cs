﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDrillerResAction : Action
{
    private ItemCardinality item;
    public override void Initialize(ItemCardinality item)
    {
        this.item = item;
    }

    public void Use()
    {
        UIDrillerControler.instance.driller.RemoveInventory(item);
        Inventory.local.Add(item.item, item.amount);

        UIInventoryControler.instance.UpdateUI();
        UIDrillerControler.instance.UpdateUI();

        Cancel();
    }

    public void Cancel()
    {
        Destroy(gameObject);
    }
}
