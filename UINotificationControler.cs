﻿using UnityEngine;
using UnityEngine.UI;

public class UINotificationControler : MonoBehaviour
{
    public static UINotificationControler instance;

    public Transform parent;
    public GameObject notificationPrefab;

    private void Start()
    {
        instance = this;
    }

    public void SetNotification(string message, float duration, Color color)
    {
        GameObject n = Instantiate(notificationPrefab, parent);
        n.GetComponent<Text>().text = message;
        n.GetComponent<Text>().color = color;
        Destroy(n, duration);

        UIChatControler.instance.AddChat(message);
    }

    public void NotifyAll(string message, float duration)
    {
        Player.local.view.RPC("RPC_BroadcastMessage", PhotonTargets.All, message, duration);
    }

    public void NotifyOthers(string message, float duration)
    {
        Player.local.view.RPC("RPC_BroadcastMessage", PhotonTargets.Others, message, duration);
    }
}
