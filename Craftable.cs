﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu( menuName = "Craftable", fileName = "Craftable")]
public class Craftable : ScriptableObject
{
    public ItemCardinality[] requirements;
    public ItemCardinality[] results;

    public bool CanCraft()
    {
        
        foreach (ItemCardinality c in requirements)
        {
            if (!Inventory.local.InventoryHasItem(c.item, c.amount)) return false;
        }

        return true;
    }

    public void Craft()
    {
        if (!CanCraft()) return;

        foreach (ItemCardinality c in results)
        {
            Inventory.local.Add(c.item, c.amount);
        }

        foreach (ItemCardinality c in requirements)
        {
            Inventory.local.Remove(c.item, c.amount);
        }

        Notify();
    }


    private void Notify()
    {
        UINotificationControler.instance.SetNotification(Player.local.name + " crafted " + results[0].item.name, 2f, Color.black);
    }

}

[System.Serializable]
public class ItemCardinality
{
    public Item item;
    public int amount;

    public ItemCardinality(Item item, int amount)
    {
        this.item = item;
        this.amount = amount;
    }
}
