﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIGeneratorResultAction : Action
{
    private ItemCardinality item;
    public override void Initialize(ItemCardinality item)
    {
        this.item = item;
    }

    public void Use()
    {
        UIGeneratorControler.instance.generator.RemoveInventory(item);
        Inventory.local.Add(item.item, item.amount);

        UIInventoryControler.instance.UpdateUI();
        UIGeneratorControler.instance.UpdateUI();

        Cancel();
    }

    public void Cancel()
    {
        Destroy(gameObject);
    }
}
