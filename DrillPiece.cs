﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrillPiece : MonoBehaviour
{
    private Driller driller;
    private FuelUsage usage;

    private List<Minable> minablesInRange;

    public bool active;

    private void Start()
    {
        driller = GetComponentInParent<Driller>();
        minablesInRange = new List<Minable>();
        usage = GetComponentInParent<FuelUsage>();
    }

    private void Update()
    {
        if (usage.IsActive() && active && !driller.InventoryIsFull()) HandleTimer();
    }

    private void HandleTimer()
    {
        for (int i = 0; i < minablesInRange.Count; i++)
        {
            if (minablesInRange[i].Mine(driller.mineSpeed))
            {
                driller.Mine(minablesInRange[i]);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Minable minable = collision.GetComponent<Minable>();

        if (minable != null)
        {
            minablesInRange.Add(minable);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Minable minable = collision.GetComponent<Minable>();

        if (minable != null)
        {
            minablesInRange.Remove(minable);
        }
    }
}
