﻿using UnityEngine;

public class Player : MonoBehaviour
{
    public PhotonView view;

    public static Player local;

    void Start() {
        view = GetComponent<PhotonView>();

        if (!view.isMine) return;

        MainCamera.instance.SetPlayer(gameObject);
        local = this;
        UINotificationControler.instance.NotifyOthers(this.name + " joined the server", 2f);
    }

    private void OnDestroy()
    {
        UINotificationControler.instance.NotifyOthers(this.name + " left the server", 2f);
    }

    public void SetName(string name)
    {
        this.name = name;

        view.RPC("RPC_SetName", PhotonTargets.AllBuffered, name);
    }

    [PunRPC]
    private void RPC_SetName(string name)
    {
        this.name = name;
    }

    [PunRPC]
    private void RPC_BroadcastMessage(string message, float duration)
    {
        UINotificationControler.instance.SetNotification(message, duration, Color.white);
    }

}
