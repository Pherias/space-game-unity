﻿using UnityEngine;

public class Minable : Interactable
{

    public float duration;
    private float currentDuration;
    public MaterialItem material;
    public int amount;

    private SpriteRenderer[] graphics;
    private PhotonView view;

    private void Start() { DoStart(); }
    private void Update() { DoUpdate(); }

    public void DoStart()
    {
        view = GetComponent<PhotonView>();

        InitGraphics();
        CancelMining();
    }

    public void DoUpdate()
    {
        if (Input.GetMouseButtonUp(0)) CancelMining();
    }

    private void InitGraphics()
    {
        graphics = GetComponentsInChildren<SpriteRenderer>();
    }

    private void UpdateGraphics()
    {
        float alpha = (duration - currentDuration) / duration;


        foreach (SpriteRenderer ren in graphics)
        {
            ren.color = new Color(ren.color.r, ren.color.g, ren.color.b, alpha);
        }

        //view.RPC("RPC_UpdateGraphics", PhotonTargets.AllBuffered, alpha);
    }

    public bool Mine(float speed)
    {
        currentDuration += Time.deltaTime * speed;

        UpdateGraphics();

        if (currentDuration >= duration)
        {
            return true;
        }

        return false;
    }

    public virtual void AfterMine()
    {
        Inventory.local.Add(material, amount);

        UINotificationControler.instance.SetNotification("+" + amount + " " + name.ToLower(), .6f, new Color(255, 255, 255));
    }

    private void CancelMining()
    {
        currentDuration = 0f;
        UpdateGraphics();
    }
}
