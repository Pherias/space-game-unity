﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICraftableRow : MonoBehaviour
{
    public Transform reqParent;
    public Transform resParent;

    private Image[] reqs;
    private Image[] res;
    private Text[] reqTexts;
    private Text[] resTexts;

    public Button craftButton;

    private Craftable craftable;

    public void Craft()
    {
        craftable.Craft();
    }

    public void Initialize(Craftable craftable)
    {
        reqs = reqParent.GetComponentsInChildren<Image>();
        res = resParent.GetComponentsInChildren<Image>();
        reqTexts = reqParent.GetComponentsInChildren<Text>();
        resTexts = resParent.GetComponentsInChildren<Text>();

        this.craftable = craftable;

        for (int i = 0; i < reqs.Length; i++)
        {
            if (i < craftable.requirements.Length)
            {
                reqs[i].sprite = craftable.requirements[i].item.image;
                reqTexts[i].text = craftable.requirements[i].amount.ToString();
                reqs[i].enabled = true;
            }
            else reqs[i].enabled = false;
        }
        
        for (int i = 0; i < res.Length; i++)
        {
            if (i < craftable.results.Length)
            {
                res[i].sprite = craftable.results[i].item.image;
                resTexts[i].text = craftable.results[i].amount.ToString();
                res[i].enabled = true;
            }
            else res[i].enabled = false;
        }

        craftButton.interactable = craftable.CanCraft();
    }

    public void Disable()
    {
        reqs = reqParent.GetComponentsInChildren<Image>();
        res = resParent.GetComponentsInChildren<Image>();
        reqTexts = resParent.GetComponentsInChildren<Text>();
        resTexts = resParent.GetComponentsInChildren<Text>();

        for (int i = 0; i < reqs.Length; i++)
        {
            reqs[i].enabled = false;
        }

        for (int i = 0; i < res.Length; i++)
        {
            res[i].enabled = false;
        }

        //craftButton.interactable = false;
    }

}
