﻿using System;
using UnityEngine;

public class Planet : Photon.MonoBehaviour
{

    new public string name;
    public float gravity;
    public float gravityDistance;
    public ResourceSpawn[] resourcesToSpawn;
    public Color color;
    public Color horizonColor;


    private CircleCollider2D collider;
    private PhotonView view;
    private Assets assets;
    private Mesh originalMesh;
    private Mesh modifiedMesh;

    void Start() {
        this.collider = GetComponent<CircleCollider2D>();
        this.gravity = 25 * transform.localScale.x;

        originalMesh = GetComponent<MeshFilter>().mesh;
        modifiedMesh = Instantiate(originalMesh);

        view = GetComponent<PhotonView>();

        assets = FindObjectOfType<Assets>();
        CreatePlanet();
    }

    private void CreatePlanet()
    {
        DigHole();
    }

    public Vector3 digPosition;
    public float holeRadius = 10f;
    public float digDepth = 20f;

    private void DigHole()
    {
         
    }

    private void InstantiateResources()
    {
        foreach (ResourceSpawn r in resourcesToSpawn)
        {
            for (int i = 0; i < r.amount; i++)
            {
                float angle = Mathf.PI * 2 * UnityEngine.Random.Range(0f, 1f);
                float x = Mathf.Sin(angle) * GetRadius() * UnityEngine.Random.Range(0.0f, 1f);
                float y = Mathf.Cos(angle) * GetRadius() * UnityEngine.Random.Range(0.0f, 1f);

                Vector3 position = new Vector3(x, y, 0) + transform.position;

                CreateResource(r.resource, position);
            }
        }
    }

    /**
     * MasterCheck is called for the master client to instantiate the world.
     */
    public void MasterCheck()
    {
        InstantiateResources();
    }

    private void CreateResource(string name, Vector3 position)
    {
        Vector3 rotateDirection = transform.position;
        float angle = Mathf.Atan2(rotateDirection.y, rotateDirection.x) * Mathf.Rad2Deg + 90;
        Quaternion rotation = Quaternion.Euler(0f, 0f, angle);

        PhotonNetwork.InstantiateSceneObject(name, position, rotation, 0, null);
    }


    public bool InRange(GameObject obj)
    {
        float distance = Vector3.Distance(obj.transform.position, transform.position);
        bool inRange = (distance <= gravityDistance);

        return inRange;
    }

    public static Planet GetClosest(Vector2 to)
    {
        Planet[] planets = FindObjectsOfType<Planet>();

        float distance = float.PositiveInfinity;
        Planet closest = null;

        foreach (Planet p in planets)
        {
            float dist = Vector2.Distance(p.transform.position, to);

            if (dist < distance)
            {
                distance = dist;
                closest = p;
            }
        }

        return closest;
    }

    public float GetRadius() {
        return this.gravity;
    }
}

[System.Serializable]
public class ResourceSpawn
{
    public string resource;
    public int amount;
}