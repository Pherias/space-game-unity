﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour
{

    private bool showMap = false;
    private float scroll = 10f;
    private float mapScroll = 0f;
    public KeyCode mapKey;

    private GameObject player;
    private Camera camera;
    public static MainCamera instance;

    public Color backgroundColor;
    private Color newColor;

    void Awake() {
        instance = this;
        player = GameObject.FindWithTag("Player");
        camera = GetComponent<Camera>();

        backgroundColor = Color.black;
        newColor = Color.black;
    }

    void Update()
    {
        if (player == null) return;

        HandlePosition();
        HandleScroll();
        HandleInput();
        HandleBackground();
    }

    private void HandleBackground()
    {

        backgroundColor = Color.Lerp(backgroundColor, newColor, Time.deltaTime);

        camera.backgroundColor = backgroundColor;

    }

    public void SetPlayer(GameObject player)
    {
        this.player = player;
    }

    private void HandleInput()
    {
        if (Input.GetKeyDown(mapKey)) showMap = !showMap;

        scroll -= Input.mouseScrollDelta.y;
        if (showMap) mapScroll -= Input.mouseScrollDelta.y;

        scroll = Mathf.Clamp(scroll, 7f, 50f);
        mapScroll = Mathf.Clamp(mapScroll, 0f, 100f);
    }

    private void HandleScroll()
    {
        if (showMap) camera.orthographicSize = 200f + scroll * 10f;
        else camera.orthographicSize = scroll;
    }

    private void HandlePosition() {
        transform.position = new Vector3(player.transform.position.x, player.transform.position.y, transform.position.z);
        
        Vector3 eulerRotation = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, player.transform.eulerAngles.z);

        transform.rotation = Quaternion.Euler(eulerRotation);
    }

    public void SetBackgroundColor(Color color)
    {
        newColor = color;
    }
}
