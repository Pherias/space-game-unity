﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGravity : Gravity
{
    private PlayerMovement playerMovement;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        view = GetComponent<PhotonView>();
        playerMovement = GetComponent<PlayerMovement>();
    }

    private void Update()
    {
        HandleGravity();
    }

    public override void HandleGravity()
    {

        if (!view.isMine) return;

        Planet closest = GetClosestPlanet();

        float distance = Vector3.Distance(closest.transform.position, transform.position);

        playerMovement.inPlanetRange = false;

        if (true)
        {

            playerMovement.inPlanetRange = true;

            Vector3 v = closest.transform.position - transform.position;

            view.RPC("RPC_AddForce", PhotonTargets.AllBuffered, v.normalized * (GetMass()) * closest.gravity);

        }
    }
}
