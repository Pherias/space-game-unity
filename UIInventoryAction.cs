﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIInventoryAction : Action
{
    private ItemCardinality item;

    public override void Initialize(ItemCardinality item)
    {
        this.item = item;
    }

    public void Use()
    {
        item.item.UseInInventory(item.amount);
        Destroy(gameObject);
    }

    public void Remove()
    {
        item.item.Discard(item.amount);
        Destroy(gameObject);
    }

    public void Cancel()
    {
        Destroy(gameObject);
    }
}
