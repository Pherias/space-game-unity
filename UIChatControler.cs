﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIChatControler : MonoBehaviour
{
    public static UIChatControler instance;

    public Transform chatbox;
    public GameObject chatLine;
    public int maxMessages;

    private int totalMessages;

    private void Start()
    {
        instance = this;
        totalMessages = 0;
    }

    public void AddChat(string line)
    {
        CancelInvoke("ClearChat");

        totalMessages++;
        if (totalMessages > maxMessages) ClearFirst();

        GameObject newLine = Instantiate(chatLine, chatbox);
        newLine.GetComponent<Text>().text = line;

        Invoke("ClearChat", 10f);
    }

    private void ClearFirst()
    {
        GameObject[] lines = GetChatLines();

        Destroy(lines[0]);
    }

    public void ClearChat()
    {
        totalMessages = 0;

        GameObject[] lines = GetChatLines();

        foreach(GameObject line in lines)
        {
            Destroy(line);
        }
    }

    public GameObject[] GetChatLines()
    {
        return GameObject.FindGameObjectsWithTag("ChatLine");
    }
}
