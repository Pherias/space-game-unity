﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public new string name;
    public Texture2D cursor;


    public Texture2D GetTexture() => cursor;

    public virtual void Interact()
    {

    }
}
