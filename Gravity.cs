﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : MonoBehaviour
{
    [SerializeField] private float mass = 1f;

    protected Rigidbody2D rb;
    protected PhotonView view;

    void Start() {
        rb = GetComponent<Rigidbody2D>();
        view = GetComponent<PhotonView>();
    }

    void Update() {
        HandleGravity();
    }

    public virtual void HandleGravity() {

        if (!view.isMine) return;

        Planet closest = GetClosestPlanet();

        float distance = Vector3.Distance(closest.transform.position, transform.position);

        if (true) {
            Vector3 v = closest.transform.position - transform.position;

            view.RPC("RPC_AddForce", PhotonTargets.AllBuffered, v.normalized * (mass) * closest.gravity); 
        }

        Vector3 rotateDirection = closest.transform.position - transform.position;
        float angle = Mathf.Atan2(rotateDirection.y, rotateDirection.x) * Mathf.Rad2Deg + 90;

        transform.rotation = Quaternion.Euler(0f, 0f, angle);
    }

    public Planet GetClosestPlanet() {
        
        Planet[] planets = GameObject.FindObjectsOfType<Planet>();

        float current = float.PositiveInfinity;
        Planet closest = null;

        foreach (Planet planet in planets)
        {
            float distance = Vector2.Distance(planet.transform.position, transform.position);

            if (distance <= current) {
                current = distance;
                closest = planet;
            }
        }

        return closest;
    }

    [PunRPC]
    private void RPC_AddForce(Vector3 vector)
    {
        GetComponent<Rigidbody2D>().AddForce(vector);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.GetComponent<Planet>() != null) {
            rb.Sleep();
        }
    }

    public float GetMass() {
        return this.mass;
    }
}
