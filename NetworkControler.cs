﻿using UnityEngine;

public class NetworkControler : Photon.MonoBehaviour
{

    public static NetworkControler instance;

    public GameObject player;

    public GameObject connectingScreen;

    [SerializeField] private string playerName;

    private void Start()
    {
        instance = this;

        PhotonNetwork.ConnectUsingSettings("1.0");

        DontDestroyOnLoad(gameObject);
        //DontDestroyOnLoad(connectingScreen);
    }

    public void JoinLobby(string playerName)
    {
        PhotonNetwork.JoinLobby();

        this.playerName = playerName;
        connectingScreen.SetActive(true);
    }

    void OnJoinedLobby()
    {
        PhotonNetwork.JoinOrCreateRoom("Space", new RoomOptions() { MaxPlayers = 15 }, TypedLobby.Default);
    }

    void OnJoinedRoom()
    {
        player = PhotonNetwork.Instantiate("Player", GameObject.FindGameObjectWithTag("Spawnpoint").transform.position, Quaternion.identity, 0);
        player.name = playerName;

        if (!PhotonNetwork.isMasterClient) Assets.instance.PlayBackgroundMusic();

        // The gamecreator (master server) needs to instantiate all server-objects.
        if (!PhotonNetwork.isMasterClient) return;

        UINotificationControler.instance.SetNotification("You are master-server", 1f, Color.white);

        Planet[] planets = FindObjectsOfType<Planet>();

        foreach (Planet p in planets)
        {
            p.MasterCheck();
        }
    }
}
