﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu( menuName = "Deployable", fileName = "Deployable")]
public class DeployableItem : Item
{
    public string resource;

    public override void UseInInventory(int amount)
    {
        GameObject o = PhotonNetwork.Instantiate(resource, Player.local.transform.position, Player.local.transform.rotation, 0, null);

        /*
         * After deployment initialize ownership and velocity across clients
         */

        Vector2 velocity = PlayerMovement.local.GetComponent<Rigidbody2D>().velocity;

        o.GetComponent<PhotonView>().RPC("RPC_AddForce", PhotonTargets.AllBuffered, new Vector3(velocity.x, velocity.y, 0));

        base.UseInInventory(1);
    }
}
