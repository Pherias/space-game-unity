﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIInventoryControler : MonoBehaviour
{

    public GameObject panel;
    public GameObject wrapper;
    public KeyCode key;
    public Button closeButton;

    [SerializeField] private InventorySlotUI[] slots;

    public static UIInventoryControler instance;

    private void Start()
    {
        slots = wrapper.GetComponentsInChildren<InventorySlotUI>();
        instance = this;
    }

    private void Update()
    {
        if (Input.GetKeyDown(key)) panel.SetActive(!panel.activeSelf);

        if (!panel.activeSelf) DestroyUIObjects();
    }

    private void DestroyUIObjects()
    {
        UIInventoryAction[] panels = FindObjectsOfType<UIInventoryAction>();

        foreach (UIInventoryAction panel in panels)
        {
            Destroy(panel.gameObject);
        }
    }

    public void UpdateUI()
    {
        Inventory inventory = Inventory.local;
        
        for (int i = 0; i < slots.Length; i++)
        {
            if (i < inventory.items.Count) slots[i].SetItem(inventory.items[i]);
            else slots[i].Disable();
        }
    }

    public void Close()
    {
        panel.SetActive(false);
        if (!panel.activeSelf) DestroyUIObjects();
    }
}
